package com.electrictravel.resolvingservice.entity;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AlgorithmEdgeTest {

    @Test
    public void correctlyCalculateLength() {
        AlgorithmNode node1 = new AlgorithmNode("Dzr", 56.2282, 43.454372);
        AlgorithmNode node2 = new AlgorithmNode("NN", 56.286279, 43.853378);

        AlgorithmEdge edge = new AlgorithmEdge(node1, node2);

        assertEquals(edge.getLength(), 25.483, 1e-3);
    }
}