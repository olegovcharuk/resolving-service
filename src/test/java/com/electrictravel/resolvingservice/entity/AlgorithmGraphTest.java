package com.electrictravel.resolvingservice.entity;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AlgorithmGraphTest {

    private AlgorithmGraph graph;

    @Before
    public void setUp() {
        graph = new AlgorithmGraph();

        graph.addNode("A", 0., 0.);
        graph.addNode("B", 0., 0.);
        graph.addNode("C", 0., 0.);
    }

    @Test
    public void correctlyAddNodes() {
        assertEquals(graph.getNodes().size(), 3);
        assertTrue(graph.getNodes().keySet().containsAll(Arrays.asList("A", "B", "C")));
    }

    @Test(expected = IllegalStateException.class)
    public void throwsAfterAddingNodeWithSameKey(){
        graph.addNode("A", 0., 0.);
    }

    @Test
    public void correctlyAddEdges() {
        graph.addEdge("A", "B");
        graph.addEdge("A", "C");

        assertEquals(graph.getNodeToEdges().size(), 3);
        assertEquals(graph.getNodeToEdges().get("A").size(), 2);
        assertEquals(graph.getNodeToEdges().get("B").size(), 1);
        assertEquals(graph.getNodeToEdges().get("C").size(), 1);
    }

}