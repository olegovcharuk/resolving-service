package com.electrictravel.resolvingservice.routes;

import com.electrictravel.resolvingservice.cache.PathCache;
import com.electrictravel.resolvingservice.cache.PathCacheRepository;
import com.electrictravel.resolvingservice.entity.AlgorithmGraph;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class RouteResolverTest {

    private static AlgorithmGraph graph;
    private RouteResolverImpl routeResolver;

    @Mock
    private PathCacheRepository cacheRepository;
    private Map<String, PathCache> cacheMap;


    @BeforeClass
    public static void initGraph() {
        graph = new AlgorithmGraph();
        graph.addNode("Moscow", 55.728368, 37.511543);
        graph.addNode("Dzr", 56.2282, 43.454372);
        graph.addNode("NN", 56.286279, 43.853378);
        graph.addNode("Murmansk", 68.972077, 33.069069);
        graph.addNode("Vladimir", 56.129807, 40.42022);

        graph.addEdge("Moscow", "Dzr");
        graph.addEdge("NN", "Dzr");
        graph.addEdge("Moscow", "Murmansk");
        graph.addEdge("NN", "Vladimir");
        graph.addEdge("Murmansk", "Vladimir");

    }

    @Before
    public void initRouteResolver() {
        MockitoAnnotations.initMocks(this);
        cacheMap = new HashMap<>();
        when(cacheRepository.findByKeyFrom(anyString())).then(new Answer<PathCache>() {
            @Override
            public PathCache answer(InvocationOnMock invocationOnMock) throws Throwable {
                String argument = invocationOnMock.getArgument(0);
                return cacheMap.get(argument);
            }
        });
        when(cacheRepository.save(any(PathCache.class))).then(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                PathCache pathCache = invocationOnMock.getArgument(0);
                cacheMap.put(pathCache.getKeyFrom(), pathCache);
                return null;
            }
        });

        routeResolver = new RouteResolverImpl(cacheRepository);
        routeResolver.init(graph);
    }

    @Test
    public void simpleRoute() {
        List<String> path = routeResolver.findShortestPathFromTo(Arrays.asList("NN"), Arrays.asList("Dzr"));

        assertNotNull(path);
        assertEquals(path.size(), 2);
        assertEquals(path.get(0), "NN");
        assertEquals(path.get(1), "Dzr");
    }

}