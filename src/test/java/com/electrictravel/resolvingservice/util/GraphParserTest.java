package com.electrictravel.resolvingservice.util;

import com.electrictravel.resolvingservice.entity.AlgorithmEdge;
import com.electrictravel.resolvingservice.entity.AlgorithmGraph;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class GraphParserTest {

    private String nodesData;
    private String edgesData;

    @Before
    public void setUp() {
        nodesData = "[{\n" +
                "        \"key\": \"s9603724\"\n" +
                "        \"direction\": \"Невельское\",\n" +
                "        \"latitude\": 56.143157,\n" +
                "        \"longitude\": 30.054376,\n" +
                "        \"station_type\": \"crossing\",\n" +
                "        \"title\": \"451 км\",\n" +
                "        \"transport_type\": \"train\"\n" +
                "    }," +
                "{\n" +
                "        \"key\": \"s9606599\"\n" +
                "        \"direction\": \"Ряжское\",\n" +
                "        \"latitude\": 53.4689551991797,\n" +
                "        \"longitude\": 41.659255047607,\n" +
                "        \"station_type\": \"crossing\",\n" +
                "        \"title\": \"Базево\",\n" +
                "        \"transport_type\": \"train\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"key\": \"s9611355\"\n" +
                "        \"direction\": \"Западное\",\n" +
                "        \"latitude\": 56.242885,\n" +
                "        \"longitude\": 114.086463,\n" +
                "        \"station_type\": \"crossing\",\n" +
                "        \"title\": \"Ульги\",\n" +
                "        \"transport_type\": \"train\"\n" +
                "    }]";

        edgesData = "[{\"from\": \"s9603724\", \"to\": \"s9606599\"}, " +
                "{\"from\": \"s9606599\", \"to\": \"s9611355\"}]";
    }

    @Test
    public void createGraph() {
        AlgorithmGraph graph = GraphParser.createGraph(nodesData, edgesData);

        assertEquals(graph.getNodes().size(), 3);
        for (String key : graph.getNodeToEdges().keySet()) {
            List<AlgorithmEdge> edges = graph.getNodeToEdges().get(key);
            int expectedEdgesCount = key.equals("s9606599") ? 2 : 1;
            assertEquals(edges.size(), expectedEdgesCount);
        }
    }
}