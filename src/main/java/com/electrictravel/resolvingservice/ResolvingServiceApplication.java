package com.electrictravel.resolvingservice;

import com.electrictravel.resolvingservice.cache.CachedGraph;
import com.electrictravel.resolvingservice.routes.RouteResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ResolvingServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ResolvingServiceApplication.class, args);
    }

    @Bean
    @Autowired
    private static int onStartup(RouteResolver routeResolver, CachedGraph cachedGraph) {
        routeResolver.init(cachedGraph.restoreAlgorithmGraph());
        return 0;
    }
}
