package com.electrictravel.resolvingservice.routes;

import com.electrictravel.resolvingservice.cache.CachedGraph;
import com.electrictravel.resolvingservice.cache.InitGraph;
import com.electrictravel.resolvingservice.entity.AlgorithmGraph;
import com.electrictravel.resolvingservice.util.GraphParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RouteController {

    private final RouteResolver routeResolver;
    private final CachedGraph cachedGraph;

    @Autowired
    public RouteController(RouteResolver routeResolver, CachedGraph cachedGraph) {
        this.routeResolver = routeResolver;
        this.cachedGraph = cachedGraph;
    }

    @RequestMapping(
            value = "/route/from/[{from}]/to/[{to}]",
            method = RequestMethod.GET)
    public List<String> getPathFromStationToStation(
            @PathVariable("from") List<String> from,
            @PathVariable("to") List<String> to
    ) {
        return routeResolver.findShortestPathFromTo(from, to);
    }

    @RequestMapping(value = "/graph/init", method = RequestMethod.POST)
    public void initGraph(@RequestBody InitGraph initGraph) {
        AlgorithmGraph graph = GraphParser.createGraph(initGraph.getStations(), initGraph.getConnections());
        routeResolver.init(graph);
        cachedGraph.updateCacheByAlgorithmGraph(graph);
    }
}
