package com.electrictravel.resolvingservice.routes;

import java.util.List;

public class Route {
    private List<String> stations;
    private double cost;

    public Route(List<String> stations, double cost) {
        this.stations = stations;
        this.cost = cost;
    }

    public List<String> getStations() {
        return stations;
    }

    public double getCost() {
        return cost;
    }
}
