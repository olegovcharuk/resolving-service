package com.electrictravel.resolvingservice.routes;

import com.electrictravel.resolvingservice.cache.PathCache;
import com.electrictravel.resolvingservice.cache.PathCacheRepository;
import com.electrictravel.resolvingservice.entity.AlgorithmEdge;
import com.electrictravel.resolvingservice.entity.AlgorithmGraph;
import com.electrictravel.resolvingservice.entity.AlgorithmNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class RouteResolverImpl implements RouteResolver {


    private AlgorithmGraph graph;
    private final PathCacheRepository cache;

    @Autowired
    public RouteResolverImpl(PathCacheRepository cache) {
        this.cache = cache;
        this.graph = null;
    }

    @Override
    public void init(AlgorithmGraph graph) {
        this.graph = graph;
    }

    @Override
    public List<String> findShortestPathFromTo(List<String> from, List<String> to) {
        if (graph == null) {
            throw new IllegalStateException("RouteResolver has no graph to work with");
        }

        double bestCost = Double.MAX_VALUE;
        Route bestRoute = null;

        for (String stationFrom : from) {
            if (cache.findByKeyFrom(stationFrom) == null) {
                findShortestPath(stationFrom);
            }
            double bestLocalCost = Double.MAX_VALUE;
            Route bestLocalRoute = null;
            Map<String, Route> routeMap = cache.findByKeyFrom(stationFrom).getRouteMap();
            for (String stationTo : to) {
                Route route = routeMap.get(stationTo);
                if (route != null && route.getCost() < bestLocalCost) {
                    bestLocalRoute = route;
                    bestLocalCost = route.getCost();
                }
            }

            if (bestLocalRoute != null && bestLocalCost < bestCost) {
                bestCost = bestLocalCost;
                bestRoute = bestLocalRoute;
            }

        }

        return bestRoute == null ? new LinkedList<>() : bestRoute.getStations();
    }

    private void findShortestPath(String start) {
        Map<String, Double> costs = new HashMap<>();
        Map<String, String> previous = new HashMap<>();

        PriorityQueue<AlgorithmNode> queue = new PriorityQueue<>(
                (o1, o2) -> costs.get(o1.getKey()) < costs.get(o2.getKey()) ? -1 : 1
        );

        for (String key : graph.getNodes().keySet()) {
            if (key.equals(start)) {
                costs.put(key, 0.);
            } else {
                costs.put(key, Double.MAX_VALUE);
            }
            queue.add(graph.getNodes().get(key));
        }

        while (!queue.isEmpty()) {
            AlgorithmNode shortest = queue.poll();
            for (AlgorithmEdge edge : graph.getNodeToEdges().get(shortest.getKey())) {
                AlgorithmNode neighbor = edge.getAssociatedNode(shortest);
                if (queue.contains(neighbor)) {
                    Double distance = costs.get(shortest.getKey()) + edge.getLength();
                    if (distance < costs.get(neighbor.getKey())) {
                        queue.remove(neighbor);
                        costs.put(neighbor.getKey(), distance);
                        queue.add(neighbor);
                        previous.put(neighbor.getKey(), shortest.getKey());
                    }
                }
            }
        }

        Map<String, Route> pathTo = new HashMap<>();
        for (String key : previous.keySet()) {
            List<String> path = new LinkedList<>();
            path.add(key);
            String temp = previous.get(key);
            while (!temp.equals(start)) {
                path.add(temp);
                temp = previous.get(temp);
            }
            path.add(start);
            Collections.reverse(path);
            pathTo.put(key, new Route(path, costs.get(key)));
        }
        cache.save(new PathCache(start, pathTo));
    }
}
