package com.electrictravel.resolvingservice.routes;

import com.electrictravel.resolvingservice.entity.AlgorithmGraph;

import java.util.List;

public interface RouteResolver {
    void init(AlgorithmGraph graph);

    List<String> findShortestPathFromTo(List<String> from, List<String> to);
}
