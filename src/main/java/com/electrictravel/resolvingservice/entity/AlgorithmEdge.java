package com.electrictravel.resolvingservice.entity;

import com.electrictravel.resolvingservice.util.Utils;

public class AlgorithmEdge {
    private final AlgorithmNode node1;
    private final AlgorithmNode node2;
    private final Double length;

    AlgorithmEdge(AlgorithmNode node1, AlgorithmNode node2) {
        this.node1 = node1;
        this.node2 = node2;
        this.length = Utils.calculateLength(node1, node2);
    }

    public AlgorithmNode getNode1() {
        return node1;
    }

    public AlgorithmNode getNode2() {
        return node2;
    }

    public AlgorithmNode getAssociatedNode(AlgorithmNode node) {
        if (node1.equals(node)) {
            return node2;
        }

        if(node2.equals(node)) {
            return node1;
        }

        throw new IllegalStateException("This node isn't associated with this edge");

    }

    public Double getLength() {
        return length;
    }
}
