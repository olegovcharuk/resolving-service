package com.electrictravel.resolvingservice.entity;

import java.util.*;

public class AlgorithmGraph {

    private final Map<String, AlgorithmNode> nodes;
    private final Map<String, List<AlgorithmEdge>> nodeToEdges;
    private final List<AbstractMap.SimpleEntry<String, String>> edges;

    public AlgorithmGraph() {
        this.nodes = new HashMap<>();
        this.nodeToEdges = new HashMap<>();
        this.edges = new LinkedList<>();
    }

    public void addNode(String key, double lat, double lng) {
        AlgorithmNode node = new AlgorithmNode(key, lat, lng);
        if (nodes.get(key) == null) {
            nodes.put(key, node);
            nodeToEdges.put(key, new LinkedList<>());
            return;
        }
        throw new IllegalStateException("AlgorithmGraph already has node with this key: " + key);

    }

    public void addEdge(String key1, String key2) {
        AlgorithmNode node1 = nodes.get(key1);
        AlgorithmNode node2 = nodes.get(key2);

        if (node1 != null && node2 != null) {
            AlgorithmEdge edge = new AlgorithmEdge(node1, node2);
            nodeToEdges.get(key1).add(edge);
            nodeToEdges.get(key2).add(edge);
            edges.add(new AbstractMap.SimpleEntry<>(key1, key2));
            return;
        }
        String notFoundedKey = (node1 == null) ? key1 : key2;
        throw new IllegalStateException("There are no associated node " + notFoundedKey + " to create edge with");
    }

    public Map<String, List<AlgorithmEdge>> getNodeToEdges() {
        return nodeToEdges;
    }

    public Map<String, AlgorithmNode> getNodes() {
        return nodes;
    }

    public List<AbstractMap.SimpleEntry<String, String>> getEdges() {
        return edges;
    }
}
