package com.electrictravel.resolvingservice.entity;

public class AlgorithmNode {
    private final String key;
    private final Double latitude;
    private final Double longitude;

    AlgorithmNode(String key, Double latitude, Double longitude) {
        this.key = key;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getKey() {
        return key;
    }

    public Double lat() {
        return latitude;
    }

    public Double lng() {
        return longitude;
    }

}
