package com.electrictravel.resolvingservice.cache;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface PathCacheRepository extends MongoRepository<PathCache, String> {
    PathCache findByKeyFrom(String keyFrom);
}
