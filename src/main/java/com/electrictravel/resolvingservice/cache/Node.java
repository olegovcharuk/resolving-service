package com.electrictravel.resolvingservice.cache;

import org.springframework.data.annotation.Id;

class Node {
    @Id
    private String key;
    private double lat;
    private double lng;

    Node(String key, double lat, double lng) {
        this.key = key;
        this.lat = lat;
        this.lng = lng;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
