package com.electrictravel.resolvingservice.cache;

public class InitGraph {
    private String stations;
    private String connections;

    public InitGraph(String stations, String connections) {
        this.stations = stations;
        this.connections = connections;
    }

    public InitGraph() {
    }

    public String getStations() {
        return stations;
    }

    public void setStations(String stations) {
        this.stations = stations;
    }

    public String getConnections() {
        return connections;
    }

    public void setConnections(String connections) {
        this.connections = connections;
    }
}
