package com.electrictravel.resolvingservice.cache;

import org.springframework.data.mongodb.repository.MongoRepository;

interface NodeRepository extends MongoRepository<Node, String> {
}
