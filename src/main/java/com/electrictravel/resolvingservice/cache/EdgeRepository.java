package com.electrictravel.resolvingservice.cache;

import org.springframework.data.mongodb.repository.MongoRepository;

interface EdgeRepository extends MongoRepository<Edge, String> {
}
