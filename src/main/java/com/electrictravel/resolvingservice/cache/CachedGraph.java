package com.electrictravel.resolvingservice.cache;

import com.electrictravel.resolvingservice.entity.AlgorithmGraph;
import com.electrictravel.resolvingservice.entity.AlgorithmNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.AbstractMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Service
public class CachedGraph {
    private NodeRepository nodeRepository;
    private EdgeRepository edgeRepository;
    private PathCacheRepository pathCacheRepository;

    @Autowired
    public CachedGraph(
            NodeRepository nodeRepository,
            EdgeRepository edgeRepository,
            PathCacheRepository pathCacheRepository) {
        this.nodeRepository = nodeRepository;
        this.edgeRepository = edgeRepository;
        this.pathCacheRepository = pathCacheRepository;
    }

    public AlgorithmGraph restoreAlgorithmGraph() {
        List<Node> nodes = nodeRepository.findAll();
        List<Edge> edges = edgeRepository.findAll();

        AlgorithmGraph algorithmGraph = new AlgorithmGraph();
        nodes.forEach(node -> {
            algorithmGraph.addNode(node.getKey(), node.getLat(), node.getLng());
        });
        edges.forEach(edge -> {
            algorithmGraph.addEdge(edge.getNode1(), edge.getNode2());
        });

        return algorithmGraph;
    }

    public void updateCacheByAlgorithmGraph(AlgorithmGraph algorithmGraph) {

        List<Node> nodesToSave = new LinkedList<>();
        List<Edge> edgesToSave = new LinkedList<>();

        Map<String, AlgorithmNode> algorithmGraphNodes = algorithmGraph.getNodes();
        algorithmGraphNodes.forEach((key, node) -> {
            nodesToSave.add(new Node(node.getKey(), node.lat(), node.lng()));
        });

        List<AbstractMap.SimpleEntry<String, String>> algorithmGraphEdges = algorithmGraph.getEdges();
        algorithmGraphEdges.forEach(edge -> {
            edgesToSave.add(new Edge(edge.getKey(), edge.getValue()));
        });

        nodeRepository.deleteAll();
        nodeRepository.save(nodesToSave);
        edgeRepository.deleteAll();
        edgeRepository.save(edgesToSave);
        pathCacheRepository.deleteAll();
    }
}
