package com.electrictravel.resolvingservice.cache;

import com.electrictravel.resolvingservice.routes.Route;
import org.springframework.data.annotation.Id;

import java.util.Map;

public class PathCache {
    @Id
    private String keyFrom;
    private Map<String, Route> routeMap;

    public PathCache(String keyFrom, Map<String, Route> routeMap) {
        this.keyFrom = keyFrom;
        this.routeMap = routeMap;
    }

    public String getKeyFrom() {
        return keyFrom;
    }

    public Map<String, Route> getRouteMap() {
        return routeMap;
    }
}
