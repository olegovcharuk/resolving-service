package com.electrictravel.resolvingservice.util;

import com.electrictravel.resolvingservice.entity.AlgorithmGraph;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Service;

@Service
public class GraphParser {

    private final static JSONParser parser = new JSONParser();

    private GraphParser() {
    }

    public static AlgorithmGraph createGraph(String jsonNodes, String jsonEdges) {
        try {
            JSONArray nodes = (JSONArray) parser.parse(jsonNodes);
            JSONArray edges = (JSONArray) parser.parse(jsonEdges);

            AlgorithmGraph result = new AlgorithmGraph();

            for (Object jsonNode : nodes) {
                JSONObject node = (JSONObject) jsonNode;
                String key = (String) node.get("key");
                Double latitude = (Double) node.get("latitude");
                Double longitude = (Double) node.get("longitude");
                result.addNode(
                        key,
                        latitude,
                        longitude
                );
            }

            for (Object jsonEdge : edges) {
                JSONObject edge = (JSONObject) jsonEdge;
                String node1 = (String) edge.get("from");
                String node2 = (String) edge.get("to");
                try {
                    result.addEdge(node1, node2);
                } catch (IllegalStateException e) {
                    System.out.println(e.getMessage());
                }
            }

            return result;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }
}
