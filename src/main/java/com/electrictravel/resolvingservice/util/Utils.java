package com.electrictravel.resolvingservice.util;

import com.electrictravel.resolvingservice.entity.AlgorithmNode;

import static java.lang.Math.*;

public class Utils {

    private static final double EARTH_RADIUS = 6372.795;

    private Utils() {}

    public static Double calculateLength(AlgorithmNode node1, AlgorithmNode node2) {
        double lat1 = node1.lat()*PI/180;
        double lat2 = node2.lat()*PI/180;
        double lng1 = node1.lng()*PI/180;
        double lng2 = node2.lng()*PI/180;

        double delta = abs(lng1 - lng2);

        double del = 2*asin(sqrt(pow(sin((lat2 - lat1)/2), 2) + cos(lat1)*cos(lat2)*pow(sin(delta/2), 2)));
        return del*EARTH_RADIUS;

    }
}
